package com.shawngl;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.PolygonShape;

public class HitHandler implements ContactListener {

	Level level;

	public HitHandler(Level level) {
		this.level = level;
	}

	private float player_hit_time = .6f;
	
	@Override
	public void beginContact(Contact contact) {
		Fixture a = contact.getFixtureA();
		Fixture b = contact.getFixtureB();
		
		if (areFixturesTouching(a, b, "feet", "ground")){
			contact.setFriction(1f);
		}
		if (areFixturesTouching(a, b, Constants.PLAYER_SWORD, Constants.SLIME)) {
			Slime slime = (Slime) getFixtureFromType(a, b, Constants.SLIME).getBody().getUserData();
			Player player = (Player) getFixtureFromType(a, b, Constants.PLAYER_SWORD).getBody().getUserData();
			if(player.setSwordAway){
				return;
			}
			slime.right = !slime.right;
			applyDamageEffect(slime, player, 2f, 2f, 1.5f);
			Assets.slimehit.play();
			return;
		}
		if (areFixturesTouching(a, b, Constants.PLAYER_SWORD, Constants.SKELETON)) {
			Skeleton skele = (Skeleton) getFixtureFromType(a, b, Constants.SKELETON).getBody().getUserData();
			Player player = (Player) getFixtureFromType(a, b, Constants.PLAYER_SWORD).getBody().getUserData();
			if(player.setSwordAway){
				return;
			}
			applyDamageEffect(skele, player, 2f, 2f, 1f);
			return;
		}
		if (areFixturesTouching(a, b, Constants.SKELETON_SWORD, Constants.PLAYER)) {
			Skeleton skele = (Skeleton) getFixtureFromType(a, b, Constants.SKELETON_SWORD).getBody().getUserData();
			Player player = (Player) getFixtureFromType(a, b, Constants.PLAYER).getBody().getUserData();
			if(skele.setSwordAway){
				return;
			}
			applyDamageEffect(player, skele, 2f, 2f, player_hit_time);
			return;
		}
		if (areFixturesTouching(a, b, Constants.PLAYER, Constants.SLIME)) {
			Slime slime = (Slime) getFixtureFromType(a, b, Constants.SLIME).getBody().getUserData();
			Player player = (Player) getFixtureFromType(a, b, Constants.PLAYER).getBody().getUserData();
			applyDamageEffect(player, slime, 2f, 2f, player_hit_time);
			return;
		}
		if (areFixturesTouching(a, b, Constants.PLAYER, Constants.SKELETON)) {
			Skeleton skele = (Skeleton) getFixtureFromType(a, b, Constants.SKELETON).getBody().getUserData();
			Player player = (Player) getFixtureFromType(a, b, Constants.PLAYER).getBody().getUserData();
			applyDamageEffect(player, skele, 3f, 2f, player_hit_time);
			return;
		}
	}

	private void applyDamageEffect(Entity target, Entity source, float knockbackx, float knockbacky, float damageTime){
		if (target.damageTimer.time <= 0f) {
			target.isFree = true;
			target.entity.setLinearVelocity((target.entity.getPosition().x <= source.entity.getPosition().x) ? -knockbackx : knockbacky, 6f);
			target.subHeart();
			target.damageTimer.time = damageTime;
			target.blinker.setBlinking(true);
		}
	}
	
	private boolean areFixturesTouching(Fixture a, Fixture b, String ae, String be) {
		if (a.getUserData() != null && a.getUserData().equals(ae) && b.getUserData() != null
				&& b.getUserData().equals(be)) {
			return true;
		} else if (b.getUserData() != null && b.getUserData().equals(ae) && a.getUserData() != null
				&& a.getUserData().equals(be)) {
			return true;
		}
		return false;
	}

	private Fixture getFixtureFromType(Fixture a, Fixture b, String type) {
		if (a.getUserData().equals(type)) {
			return a;
		} else if (b.getUserData().equals(type)) {
			return b;
		}
		return null;
	}

	@Override
	public void endContact(Contact contact) {

	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		Fixture a = contact.getFixtureA();
		Fixture b = contact.getFixtureB();
		float ball_y = 0, ypos = 0.8f;
        if(areFixturesTouching(a, b, "feet", "ground")){	
        	Vector2 platform_top_left = new Vector2();
        	((PolygonShape) getFixtureFromType(a, b, "ground").getShape()).getVertex(3, platform_top_left);
        	ball_y = getFixtureFromType(a, b, "feet").getBody().getPosition().y;
            if(ball_y <= platform_top_left.y + ypos + 0.1f) {  
            	contact.setEnabled(false);
            }
        }
        if(areFixturesTouching(a, b, "player", "ground")){
        	Vector2 platform_top_left = new Vector2();
        	((PolygonShape) getFixtureFromType(a, b, "ground").getShape()).getVertex(3, platform_top_left);
        	ball_y = getFixtureFromType(a, b, "player").getBody().getPosition().y;
            if(ball_y <= platform_top_left.y + ypos){  
            	contact.setEnabled(false);
            }
        }
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse){
	}
}
