package com.shawngl;

public class Constants {
	
	public static final String PLAYER = "player"; 
	public static final String GROUND = "ground"; 
	public static final String SLIME = "slime"; 
	public static final String SKELETON = "skeleton";
	public static final String PLAYER_SWORD = "sword"; 
	public static final String SKELETON_SWORD = "skeleton_sword"; 
	
	
	public static final short PLAYER_BIT = 0x0001;
	public static final short GROUND_BIT = 0x0002;
	public static final short SLIME_BIT = 0x0004;
	public static final short SKELETON_BIT = 0x0008;
	public static final short COMMON_ENEMY_BIT = 0X0016;
	public static final short PLATFORM_BIT_TEMP = 0x0032;
	

}
