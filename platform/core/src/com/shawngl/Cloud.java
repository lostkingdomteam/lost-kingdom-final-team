package com.shawngl;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class Cloud {
	Vector2 position;
	float xvel;
	private Texture cloud_image;
	boolean toRemove = false;
	
	public Cloud(float xvel, float xpos, float ypos){
		position = new Vector2(xpos, ypos);
		this.xvel = xvel;
		cloud_image = Assets.clouds[MathUtils.random(4)];
	}
	public void update(SpriteBatch b, float delta){
		position.x += (xvel * delta);
		if(position.x <= -500 || position.x >= 500){
			toRemove = true;
		}
		b.draw(cloud_image, position.x, position.y);
	}
}
