package com.shawngl;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Credits implements Screen{
	
	Game game;
	
	public Credits(Game game){
		this.game = game;
	}

	private String shawn = "Shawn Greene-LaFrance";
	private String pat = "Patrick Beaulac";
	
	BitmapFont font = Assets.FONT;
	
	SpriteBatch batch;
	
	float stateTime = 0f;
	@Override
	public void show(){
		batch = new SpriteBatch();
	}

	@Override
	public void render(float delta) {
	
		batch.begin();
			font.draw(batch, shawn, 10, 440);
			font.draw(batch, pat, 10, 470);

			batch.draw(Assets.moving_.getKeyFrame(stateTime+=delta,true), 100, 60,300, 300);
		batch.end();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose(){
		
	}

}
