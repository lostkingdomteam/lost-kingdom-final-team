package com.shawngl;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

public class DeathEffect {
	public DeathEffect(float x, float y){
		death.add(this);
		this.x = x;
		this.y = y;
	}
	public static Array<DeathEffect> death = new Array<DeathEffect>();
	
	private float x, y;
	
	Timer animationTime = new Timer();
	public static void removeFinishedAnimation(){
		for(int i = 0; i < death.size; i++){
			if(Assets.death_smoke_.isAnimationFinished(death.get(i).animationTime.time))
				death.removeIndex(i);
		}
	}
	public static void animate(SpriteBatch b, float delta){
		for(int i = 0; i < death.size; i++){
			DeathEffect de = death.get(i);
			b.draw(Assets.death_smoke_.getKeyFrame(de.animationTime.time+=delta*1.5f, false), de.x-1.8f, de.y-1f, 4f, 3f);
		}
	}
}
