package com.shawngl;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Array;

public abstract class Entity {
	
	Timer damageTimer = new Timer();
	Blinker blinker = new Blinker(1.2f);
	public boolean isTaggedDeath= false;
	public boolean isFree = false;
	protected Level worldLevel;
	
	protected float animationTime = 0f;
	public boolean right = true;
	
	public static Array<Entity> entities = new Array<Entity>();
	
	protected Body entity;
	
	public Entity(Body body, Level level){
		this.entity = body;
		this.worldLevel = level;
		entity.setUserData(this);
		entities.add(this);
	}
	
	protected int health;
	
	public abstract void update(float delta, SpriteBatch batch);
	public abstract void render(float delta, SpriteBatch batch);
	
	public abstract void die();
	
	public void subHeart(){
		if(--health <= 0)
			die();
	}
}
