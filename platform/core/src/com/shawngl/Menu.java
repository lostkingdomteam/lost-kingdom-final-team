package com.shawngl;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;

public class Menu implements Screen{

	/**
	 * SHAWNGL (C)
	 * 
	 * Platforming adventure by Shawn Greene-LaFrance
	 */
	
	private Game gameRef;
	
	public Menu(Game game){		
		this.gameRef = game;
	}
	
	
	SpriteBatch batch, graphics, back;
	ShapeRenderer sr;
	OrthographicCamera camera, bg, screen;
	
	FadeEffect effect;
	
	public final CharSequence TITLE = "The LOST Kingdom";
	private Array<Cloud> clouds = new Array<Cloud>();
	
	@Override
	public void show() {
		
		effect = new FadeEffect(false);
		
		screen = new OrthographicCamera(320, 240);
		camera = new OrthographicCamera();
		bg = new OrthographicCamera();
		bg.setToOrtho(false, 384, 224);
		camera.setToOrtho(false, 320, 240);
		sr = new ShapeRenderer();
		batch = new SpriteBatch();
		graphics = new SpriteBatch();
		back = new SpriteBatch();
		
		Assets.FONT_TITLE.setColor(Color.GOLDENROD);
		Assets.FONT_TITLE.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Nearest);
		
		//Assets.menu.play();
	}

	float shouldUpdate = 0f;
	
	private void addCloud(float delta){
		if((shouldUpdate+=delta) >= 2f){
			shouldUpdate = 0f;
			clouds.add(new Cloud(-100f, 400, MathUtils.random(160, 200)));
		}
	}
	
	float xpos = 0f;
	float alpha = 1f;
	
	@Override
	public void render(float delta) {
		batch.setProjectionMatrix(camera.combined);
		back.setProjectionMatrix(bg.combined);
		sr.setProjectionMatrix(camera.combined);
		
		back.begin();
			back.draw(Assets.background, 0, 0, (int)(xpos += 3)/4, 0, 384, 224);
			back.draw(Assets.forest, 0, -20, (int)(xpos += 3)/2, 0, 384, 224);
			for(Cloud c : clouds){
				c.update(back, delta);
				if(c.toRemove)
					clouds.removeValue(c, false);
			}
			addCloud(delta);
		back.end();
		
		batch.begin();
			batch.draw(Assets.text_back, 2f, (camera.viewportHeight/2-1.5f)+13, 316f, 45f);
			Assets.FONT_TITLE.draw(batch, TITLE, camera.viewportWidth/2f - (TITLE.length() * 16f) * .58f +3f, (camera.viewportHeight/2 + 35)+13);
		batch.end();
		
		if(!effect.done){
			effect.update(camera, delta);
		}
		else if(Gdx.input.isKeyPressed(Keys.SPACE)){
			gameRef.setScreen(Platformer.overworld);
		}
	}

	@Override
	public void resize(int width, int height) {}

	@Override
	public void pause() {}

	@Override
	public void resume() {}

	@Override
	public void hide() {}

	@Override
	public void dispose() {}

}
