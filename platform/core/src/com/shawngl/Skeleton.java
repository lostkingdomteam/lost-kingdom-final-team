package com.shawngl;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

public class Skeleton extends Player {

	TextureRegion currentFrame;
	Body playerBody;
	Timer strike_time = new Timer();
	Timer idle_time = new Timer();

	private static float IDLE_TIME = 1.7f;
	private static float SPEED = 2f;

	boolean inRange = false;

	public Skeleton(Body body, Level level) {
		super(body, level);
		playerBody = level.playerBody;
		right = false;
		health = 2;
		idle_time.time = 0;
	}

	@Override
	public void update(float delta, SpriteBatch batch) {
		// Logic
		Vector2 vel = entity.getLinearVelocity();
		Vector2 pos = entity.getPosition();
		if (inRange) {
			if (!isFree) {
				blinker.setBlinking(false);
				if (inRange && idle_time.time <= 0f) {
					if (!right && vel.x > -2f) {
						entity.applyLinearImpulse(-SPEED, 0f, pos.x, pos.y, true);
					} else if (right && vel.x < 2f) {
						entity.applyLinearImpulse(SPEED, 0f, pos.x, pos.y, true);
					}
					if (playerBody.getPosition().x > pos.x) {
						right = true;
					} else {
						right = false;
					}
					if(((strike_time.time += delta) > 1f) && !isAttacking) {
						isAttacking = true;
						strike_time.time = 0;
					}
				} else if (idle_time.time > 0f) {
					idle_time.time -= delta;
				}

				// Draw
				if (!isAttacking) {
					if (entity.getLinearVelocity().y > 0f || entity.getLinearVelocity().y < 0f) {
						if (entity.getLinearVelocity().y > 0f) {
							if (right)
								currentFrame = Assets.skeleton_pos_.getKeyFrame(animationTime += delta, true);
							else
								currentFrame = Assets.skeleton_pos_left_.getKeyFrame(animationTime += delta, true);
						} else if (entity.getLinearVelocity().y < 0f) {
							if (right)
								currentFrame = Assets.skeleton_neg_.getKeyFrame(animationTime += delta, true);
							else
								currentFrame = Assets.skeleton_neg_left_.getKeyFrame(animationTime += delta, true);
						}
					} else if (entity.getLinearVelocity().x > 0f || entity.getLinearVelocity().x < 0f) {
						if (right)
							currentFrame = Assets.skeleton_move_.getKeyFrame(animationTime += delta, true);
						else
							currentFrame = Assets.skeleton_move_left_.getKeyFrame(animationTime += delta, true);
					} else {
						if (right)
							currentFrame = Assets.skeleton_idle_.getKeyFrame(animationTime += delta, true);
						else
							currentFrame = Assets.skeleton_idle_left_.getKeyFrame(animationTime += delta, true);
					}
				} else {
					if (right) {
						currentFrame = Assets.skeleton_attack_.getKeyFrame(attack_time.time += delta * 1.5f, false);
						if (Assets.skeleton_attack_.isAnimationFinished(attack_time.time)) {
							isAttacking = false;
							attack_time.time = 0f;
							entity.destroyFixture(sword);
							isStriking = false;
							entity.applyLinearImpulse(0f, 4f, pos.x, pos.y, true);
							idle_time.time = IDLE_TIME;
						} else if (Assets.attack_.getKeyFrame(attack_time.time)
								.equals(Assets.attack_.getKeyFrames()[1])) {
							if (!isStriking) {
								generateSwordBox();
							}
							isStriking = true;
						}
					} else {
						currentFrame = Assets.skeleton_attack_left_.getKeyFrame(attack_time.time += delta * 1.5f,
								false);
						if (Assets.skeleton_attack_left_.isAnimationFinished(attack_time.time)) {
							isAttacking = false;
							attack_time.time = 0f;
							entity.destroyFixture(sword);
							isStriking = false;
							entity.applyLinearImpulse(0f, 6f, pos.x, pos.y, true);
							idle_time.time = IDLE_TIME;
						} else if (Assets.attack_left_.getKeyFrame(attack_time.time)
								.equals(Assets.attack_left_.getKeyFrames()[1])) {
							if (!isStriking) {
								generateSwordBox();
							}
							isStriking = true;
						}
					}
				}
			} else {
				if (blinker.shouldBlink(delta)) {
					return;
				}
				if (right) {
					currentFrame = Assets.skeleton_neg_.getKeyFrame(animationTime += delta, true);
				} else {
					currentFrame = Assets.skeleton_neg_left_.getKeyFrame(animationTime += delta, true);
				}
				damageTimer.time -= delta;
				if (damageTimer.time <= 0) {
					isFree = false;
					idle_time.time = 0f;
				}
			}
			if (right)
				batch.draw(currentFrame, entity.getPosition().x - 1.5f, entity.getPosition().y - 1f,
						currentFrame.getRegionWidth() / 5f, currentFrame.getRegionHeight() / 8f);
			else {
				float xset = 2.2f;
				if (currentFrame.getTexture().equals(Assets.skeleton_neg_left)) {
					xset = 1f;
				} else if (currentFrame.getTexture().equals(Assets.skeleton_attack_left)) {
					xset = 4f;
				}
				else if(currentFrame.getTexture().equals(Assets.skeleton_idle_left)){
					xset = 1.2f;
				}
				else if(currentFrame.getTexture().equals(Assets.skeleton_pos_left) || currentFrame.getTexture().equals(Assets.skeleton_neg_left)){
					xset = 1f;
				}
				batch.draw(currentFrame, entity.getPosition().x - xset, entity.getPosition().y - 1f,
						currentFrame.getRegionWidth() / 5f, currentFrame.getRegionHeight() / 8f);
			}
		}
		else{
			if(pos.dst2(worldLevel.player.entity.getPosition()) <= 150f){
				inRange = true;
			}
		}
	}

	private void generateSwordBox() {
		FixtureDef fd = new FixtureDef();

		Vector2 middle = entity.getLocalCenter();

		PolygonShape shape = new PolygonShape();
		if (right) {
			shape.set(
					new Vector2[] { new Vector2(middle.x + 3.9f, middle.y), new Vector2(middle.x + 1f, middle.y - .5f),
							new Vector2(middle.x + 3.9f, middle.y - .5f), new Vector2(middle.x + 1f, middle.y) });
		} else {
			shape.set(
					new Vector2[] { new Vector2(middle.x - 3.9f, middle.y), new Vector2(middle.x - 1f, middle.y - .5f),
							new Vector2(middle.x - 3.9f, middle.y - .5f), new Vector2(middle.x - 1f, middle.y) });
		}

		fd.isSensor = true;
		fd.shape = shape;

		Fixture f = entity.createFixture(fd);
		f.setUserData("skeleton_sword");

		sword = f;
		shape.dispose();
	}

	@Override
	public void die() {
		isTaggedDeath = true;
		Assets.slimedie.play();
		worldLevel.score += 200;
	}

}