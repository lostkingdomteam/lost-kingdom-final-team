package com.shawngl;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;

public class Platformer extends Game {
	
	public static Screen menu, overworld, credits;
	
	@Override
	public void create () {
		//Load assets
		Assets.load();
		//Create overworld screen
		overworld = new Overworld(this);
		//Create menu screen
		menu = new Menu(this);
		//Create credits screen
		credits = new Credits(this);
		//set the screen to the menu
		setScreen(new Level(this, LevelType.Grass, Assets.flaylayout));
	}

	@Override
	public void render () {
		//Do global debugging here!!
		Gdx.graphics.setTitle("FPS: " + Gdx.graphics.getFramesPerSecond());
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		super.render();
	}
}
