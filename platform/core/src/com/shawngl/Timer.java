package com.shawngl;

public class Timer {
	
	public float time = 0f;
	public int seconds = 0;
	
	public void updateTime(float delta){
		time += delta;
		if(time >= 1){
			seconds++;
			time = 0f;
		}
	}

}
