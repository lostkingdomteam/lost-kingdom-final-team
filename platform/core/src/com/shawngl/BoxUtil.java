package com.shawngl;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

public class BoxUtil {
	
	World world;
	Level level;
	TiledMap currentMap;
	
	public BoxUtil(World w, Level level){
		world = w;
		this.level = level;
	}
	//Used for platforms
	public Body addStaticBox(float x, float y, float width, float height){
		BodyDef def = new BodyDef();
		def.position.set(x, y);
		def.type = BodyType.StaticBody;
		Body body = world.createBody(def);
		PolygonShape s = new PolygonShape();
		s.setAsBox(width, height-0.15f);
		FixtureDef fd = new FixtureDef();
		fd.shape = s;
		fd.filter.categoryBits = Constants.GROUND_BIT;
		fd.filter.maskBits = Constants.SKELETON_BIT;
		Fixture box = body.createFixture(fd);
		box.setFriction(1f);
		body.setUserData("ground");
		body.resetMassData();
		
		s.dispose();
		return body;
	}
	public Body addPlayer(float x, float y){
		
		Body player;
		
		BodyDef playerDef = new BodyDef();
		playerDef.type = BodyType.DynamicBody;
		playerDef.fixedRotation = true;
		playerDef.position.set(x,y);
		player = world.createBody(playerDef);
		
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(0.9f, 0.9f);
		FixtureDef player_body = new FixtureDef();
		player_body.shape = shape;
		player_body.filter.categoryBits = Constants.PLAYER_BIT;
		player_body.filter.maskBits = Constants.GROUND_BIT | Constants.SKELETON_BIT | ~Constants.PLATFORM_BIT_TEMP;
		Fixture box = player.createFixture(shape, 0f);
		box.setUserData(Constants.PLAYER);
		player.resetMassData();
		
		PolygonShape feet = new PolygonShape();
		feet.setAsBox(.89f, .91f);
		FixtureDef fd = new FixtureDef();
		fd.shape = feet;
		fd.filter.categoryBits = Constants.PLAYER_BIT;
		fd.filter.maskBits = Constants.GROUND_BIT | ~Constants.PLATFORM_BIT_TEMP;
		fd.friction = 1f;
		fd.density = 0f;
		Fixture fix = player.createFixture(fd);
		fix.setUserData("feet");
		
		shape.dispose();
		feet.dispose();
		
		level.player = new Player(player, level);
		return player;
	}
	public Body addSlime(float x, float y){
		Body slime;
		
		BodyDef slimeDef = new BodyDef();
		slimeDef.type = BodyType.DynamicBody;
		slimeDef.fixedRotation = true;
		slimeDef.position.set(x, y);
		
		slime = world.createBody(slimeDef);
		
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(0.8f, 0.6f);
		FixtureDef sf = new FixtureDef();
		sf.filter.categoryBits = Constants.SLIME_BIT;
		sf.filter.maskBits = (Constants.GROUND_BIT | Constants.PLAYER_BIT) & ~Constants.SLIME_BIT;
		sf.shape = shape;
		Fixture box = slime.createFixture(sf);
		box.setFriction(0f);
		box.setRestitution(0.9f);
		box.setDensity(0f);
		box.setUserData("slime");
		
		shape.dispose();
		slime.resetMassData();
		
		slime.setUserData("slime");
			
		return slime;
	}
	public Body addSkeleton(float x, float y){
		Body skeleton;
		
		BodyDef skeletonDef = new BodyDef();
		skeletonDef.type = BodyType.DynamicBody;
		skeletonDef.fixedRotation = true;
		skeletonDef.position.set(x,y);
		skeleton = world.createBody(skeletonDef);
		
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(0.9f, 0.9f);
		FixtureDef body_def = new FixtureDef();
		body_def.shape = shape;
		body_def.filter.categoryBits = Constants.SKELETON_BIT;
		body_def.filter.maskBits = Constants.GROUND_BIT | Constants.PLAYER_BIT;
		Fixture box = skeleton.createFixture(body_def);
		box.setUserData(Constants.SKELETON);
		skeleton.resetMassData();
		
		PolygonShape feet = new PolygonShape();
		feet.setAsBox(.89f, .91f);
		FixtureDef fd = new FixtureDef();
		fd.filter.categoryBits = Constants.SKELETON_BIT;
		fd.filter.maskBits = Constants.GROUND_BIT;
		fd.shape = feet;
		fd.friction = 1f;
		fd.density = 0f;
		Fixture fix = skeleton.createFixture(fd);
		fix.setUserData("feet");
		
		feet.dispose();
		shape.dispose();
		
		return skeleton;
	}
	public Body addMage(float x, float y){
		Body mage;
		
		BodyDef mageDef = new BodyDef();
		mageDef.type = BodyType.DynamicBody;
		mageDef.fixedRotation = true;
		mageDef.position.set(x, y);
		
		mage = world.createBody(mageDef);
		
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(0.8f, 0.6f);
		FixtureDef sf = new FixtureDef();
		sf.isSensor = true;
		sf.shape = shape;
		Fixture box = mage.createFixture(sf);
		box.setFriction(0f);
		box.setDensity(0f);
		box.setUserData("slime");
		
		shape.dispose();
		
		mage.setUserData("mage");
			
		return mage;	
	}
	public static TiledMap getMap(String loc){
		return new TmxMapLoader().load(Gdx.files.local(loc).path());
	}
	public void setMap(TiledMap map){
		if(currentMap != null){
			currentMap.dispose();
		}
		currentMap = map;
	}
}
