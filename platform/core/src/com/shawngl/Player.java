package com.shawngl;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

public class Player extends Entity {

	public static boolean DEAD = false;
	
	
	public Player(Body body, Level l) {
		super(body, l);
		health = 3;
	}

	TextureRegion currentFrame = Assets.jump_neg_.getKeyFrames()[0];
	
	protected Timer attack_time = new Timer();
	protected boolean isAttacking = false;
	protected boolean isStriking = false;
	protected boolean setSwordAway = false;
	protected Fixture sword;
	protected int attack_frame = 0;

	@Override
	public void update(float delta, SpriteBatch batch) {
		// Logic
		
		Vector2 vel = entity.getLinearVelocity();
		Vector2 pos = entity.getPosition();
		
		if((pos.y + 3f) < 0){
			die();
		}

		
		if (!isFree) {
			setSwordAway = false;
			blinker.setBlinking(false);
			if ((Gdx.input.isKeyPressed(Keys.D)) && vel.x < 5f && !Gdx.input.isKeyPressed(Keys.A) && !isAttacking) {
				entity.applyLinearImpulse(1f, 0, pos.x, pos.y, true);
				right = true;
			}
			else if (Gdx.input.isKeyPressed(Keys.A) && vel.x > -5f && !isAttacking) {
				entity.applyLinearImpulse(-1f, 0, pos.x, pos.y, true);
				right = false;
			}
			if (Gdx.input.isKeyJustPressed(Keys.SPACE) && !isAttacking) {
				if (entity.getLinearVelocity().y == 0f) {
					entity.applyLinearImpulse(0f, 7f, pos.x, pos.y, true);
				}
			}
			else if(Gdx.input.isKeyJustPressed(Keys.S) && vel.y == 0f){
				System.out.println("We should fall through the floor now.");
				entity.setTransform(entity.getPosition().add(0, -0.3f), 0f);
				entity.applyLinearImpulse(0f, -0.1f, pos.x, pos.y, true);
			}
			if (Gdx.input.isKeyJustPressed(Keys.COMMA) && !isAttacking) {
				Assets.swing.play();
				isAttacking = true;
			}
			// Draw
			if (!isAttacking) {
				if (entity.getLinearVelocity().y > 0f || entity.getLinearVelocity().y < 0f) {
					if (entity.getLinearVelocity().y > 0f) {
						if (right)
							currentFrame = Assets.jump_pos_.getKeyFrame(animationTime += delta, true);
						else
							currentFrame = Assets.jump_pos_left_.getKeyFrame(animationTime += delta, true);
					} 
					else if (entity.getLinearVelocity().y < 0f) {
						if (right)
							currentFrame = Assets.jump_neg_.getKeyFrame(animationTime += delta, true);
						else
							currentFrame = Assets.jump_neg_left_.getKeyFrame(animationTime += delta, true);
					}
				} 
				else if (entity.getLinearVelocity().x > 0f || entity.getLinearVelocity().x < 0f) {
					if (right)
						currentFrame = Assets.moving_.getKeyFrame(animationTime += delta, true);
					else
						currentFrame = Assets.moving_left_.getKeyFrame(animationTime += delta, true);
				} 
				else {
					if (right)
						currentFrame = Assets.idle_.getKeyFrame(animationTime += delta, true);
					else
						currentFrame = Assets.idle_left_.getKeyFrame(animationTime += delta, true);
				}
			} 
			else {
				if (right) {
					currentFrame = Assets.attack_.getKeyFrame(attack_time.time += delta * 1.5f, false);
					if (Assets.attack_.isAnimationFinished(attack_time.time)) {
						isAttacking = false;
						attack_time.time = 0f;
						if(sword != null){
							entity.destroyFixture(sword);
						}
						isStriking = false;

					} else if (Assets.attack_.getKeyFrame(attack_time.time).equals(Assets.attack_.getKeyFrames()[1])) {
						if (!isStriking) {
							generateSwordBox();
						}
						isStriking = true;
					}
				} 
				else {
					currentFrame = Assets.attack_left_.getKeyFrame(attack_time.time += delta * 1.5f, false);
					if (Assets.attack_left_.isAnimationFinished(attack_time.time)) {
						isAttacking = false;
						attack_time.time = 0f;
						if(sword != null){
							entity.destroyFixture(sword);
						}
						isStriking = false;
					} else if (Assets.attack_left_.getKeyFrame(attack_time.time)
							.equals(Assets.attack_left_.getKeyFrames()[1])) {
						if (!isStriking) {
							generateSwordBox();
						}
						isStriking = true;
					}
				}
			}
		} 
		else {
			setSwordAway = true;
			if (blinker.shouldBlink(delta)) {
				return;
			}
			if (right)
				currentFrame = Assets.hit_.getKeyFrame(animationTime += delta, true);
			else {
				currentFrame = Assets.hit_left_.getKeyFrame(animationTime += delta, true);
			}
			damageTimer.time -= delta;
			if (damageTimer.time <= 0)
				isFree = false;
		}
		if (right)
			batch.draw(currentFrame, entity.getPosition().x - 1.5f, entity.getPosition().y - .925f,
					currentFrame.getRegionWidth() / 5f, currentFrame.getRegionHeight() / 8f);
		else {
			float xset = 1.5f;
			if (currentFrame.getTexture().equals(Assets.attack_left)) {
				xset = 4f;
			}
			else if (currentFrame.getTexture().equals(Assets.hit_left)) {
				xset = 3f;
			}
			batch.draw(currentFrame, entity.getPosition().x - xset, entity.getPosition().y - .93f,
					currentFrame.getRegionWidth() / 5f, currentFrame.getRegionHeight() / 8f);
		}

	}

	@Override
	public void render(float delta, SpriteBatch batch) {
		
	}
	private void generateSwordBox() {
		FixtureDef fd = new FixtureDef();

		Vector2 middle = entity.getLocalCenter();

		PolygonShape shape = new PolygonShape();
		if (right) {
			shape.set(
					new Vector2[] { new Vector2(middle.x + 3.9f, middle.y), new Vector2(middle.x + 1f, middle.y - .5f),
							new Vector2(middle.x + 3.9f, middle.y - .5f), new Vector2(middle.x + 1f, middle.y) });
		} else {
			shape.set(
					new Vector2[] { new Vector2(middle.x - 3.9f, middle.y), new Vector2(middle.x - 1f, middle.y - .5f),
							new Vector2(middle.x - 3.9f, middle.y - .5f), new Vector2(middle.x - 1f, middle.y) });
		}

		fd.isSensor = true;
		fd.shape = shape;

		Fixture f = entity.createFixture(fd);
		f.setUserData("sword");

		sword = f;
		shape.dispose();
	}

	@Override
	public void die() {
		Assets.slimedie.play();
		isTaggedDeath = true;
		DEAD = true;
		worldLevel.effect = new FadeEffect(true);
		Assets.level1.stop();
	}
}