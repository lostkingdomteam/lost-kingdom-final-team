package com.shawngl;

public enum LevelType {
	Forest, Town, Cave, Mountain, Castle, Grass
}
