package com.shawngl;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

public class Assets {
	
	public static String RESOURCE = "resources/";
	
	//Hero Sprites
	public static Texture idle, idle_left, moving, moving_left, jump_pos, jump_neg, jump_pos_left, jump_neg_left, attack, attack_left, hit, hit_left, skeleton_idle, skeleton_idle_left, skeleton_move, skeleton_move_left, skeleton_pos, skeleton_neg, skeleton_pos_left, skeleton_neg_left, skeleton_attack, skeleton_attack_left;
	public static Animation idle_, idle_left_, moving_, moving_left_, jump_neg_, jump_pos_, jump_neg_left_, jump_pos_left_, attack_, attack_left_, hit_, hit_left_, skeleton_idle_, skeleton_idle_left_, skeleton_move_, skeleton_move_left_, skeleton_pos_, skeleton_neg_, skeleton_pos_left_, skeleton_neg_left_, skeleton_attack_, skeleton_attack_left_;
	//Art sets NOT used for TiledMaps
	public static Texture background, background2, background2_over, background3, background3_over, forest, bAttack, bLeft, bRight, bUp, death_smoke, text_back, heart;
	public static Texture[] clouds;
	public static Animation death_smoke_;
	//Enemies
	public static Texture slime;
	public static Animation slime_;
	//Sounds
	public static Sound swing, slimehit, slimedie;
	//Music
	public static Music menu, level1;
	//Levels
	public static TiledMap flaylayout, castle2, outside1;
	//Overworlds & Assets
	public static TiledMap overworld;
	public static Texture ow_char;
	public static Animation ow_char_, ow_char_up, ow_char_down, ow_char_left, ow_char_right;
	
	public static Texture random_tga;
	
	//Pixels per meter
	public final static float PPM = 16f;
	//Font
	public static BitmapFont FONT, FONT_TITLE;

	public static TiledMap hormap;
	
	public static void load(){
		swing = Gdx.audio.newSound(Gdx.files.internal(RESOURCE + "sound/swing.wav"));
		slimehit = Gdx.audio.newSound(Gdx.files.internal(RESOURCE +"sound/slimehit.wav"));
		slimedie = Gdx.audio.newSound(Gdx.files.internal(RESOURCE +"sound/slimedie.wav"));
		
		level1 = Gdx.audio.newMusic(Gdx.files.internal(RESOURCE +"music/grasslands2.ogg"));
		level1.setLooping(true);
		
		menu = Gdx.audio.newMusic(Gdx.files.internal(RESOURCE +"music/menu.mp3"));
		menu.setLooping(true);
		
		text_back = new Texture(RESOURCE +"text_back.png");
		heart = new Texture(Gdx.files.internal(RESOURCE +"heart.png"));
		
		FONT = new BitmapFont(Gdx.files.internal(RESOURCE +"font/alagard.fnt"));
		FONT_TITLE = new BitmapFont(Gdx.files.internal(RESOURCE +"font/alagard.fnt"));
		
		clouds = new Texture[]{
			new Texture(RESOURCE +"maps/mapart/cloud1.png"),
			new Texture(RESOURCE +"maps/mapart/cloud2.png"),
			new Texture(RESOURCE +"maps/mapart/cloud3.png"),
			new Texture(RESOURCE +"maps/mapart/cloud4.png"),
			new Texture(RESOURCE +"maps/mapart/cloud5.png")
		};
		
		death_smoke = new Texture(RESOURCE +"death-strip.png");
		death_smoke_ = asAnimation(death_smoke, 28, 28, 1/10f);
		
		skeleton_idle = new Texture(RESOURCE +"skeleton-idle-strip.png");
		skeleton_idle_left = new Texture(RESOURCE +"skeleton-idle_left-strip.png");
		
		skeleton_idle_ = asAnimation(skeleton_idle, 19, 19);
		skeleton_idle_left_ = asAnimation(skeleton_idle_left, 19, 19);
		
		skeleton_move = new Texture(RESOURCE +"skeleton-move-strip.png");
		skeleton_move_left = new Texture(RESOURCE +"skeleton-move_left-strip.png");
		
		skeleton_move_ = asAnimation(skeleton_move, 17, 21);
		skeleton_move_left_ = asAnimation(skeleton_move_left, 17, 21);
		skeleton_move_left_.setPlayMode(PlayMode.REVERSED);
		
		skeleton_pos = new Texture(RESOURCE +"skeleton-jump.png");
		skeleton_neg = new Texture(RESOURCE +"skeleton-fall.png");
		
		skeleton_pos_ = asAnimation(skeleton_pos, 15, 22);
		skeleton_neg_ = asAnimation(skeleton_neg, 10, 18);
		
		skeleton_pos_left = new Texture(RESOURCE +"skeleton-jump_left.png");
		skeleton_neg_left = new Texture(RESOURCE +"skeleton-fall_left.png");
		
		skeleton_pos_left_ = asAnimation(skeleton_pos_left, 15, 22);
		skeleton_neg_left_ = asAnimation(skeleton_neg_left, 10, 18);
		
		skeleton_attack = new Texture(RESOURCE +"skeleton-attack-strip.png");
		skeleton_attack_left = new Texture(RESOURCE +"skeleton-attack_left-strip.png");
		
		skeleton_attack_ = asAnimation(skeleton_attack, 26, 19);
		skeleton_attack_left_ = asAnimation(skeleton_attack_left, 26, 19);
		skeleton_attack_left_.setPlayMode(PlayMode.REVERSED);
		
		idle = new Texture(RESOURCE +"hero_idle2-strip.png");
		idle_ = asAnimation(idle, 16, 19);
		
		idle_left = new Texture(RESOURCE +"hero_idle_left2-strip.png");
		idle_left_ = asAnimation(idle_left, 16, 19);
		
		hit = new Texture(RESOURCE +"hit.png");
		hit_ = asAnimation(hit, 22, 17);
		
		hit_left = new Texture(RESOURCE +"hit_left.png");
		hit_left_ = asAnimation(hit_left, 22, 17);
		
		moving = new Texture(RESOURCE +"moving2-strip.png");
		moving_ = asAnimation(moving, 16, 21);
		
		moving_left = new Texture(RESOURCE +"moving_left2-strip.png");
		moving_left_ = asAnimation(moving_left, 16, 21);
		moving_left_.setPlayMode(PlayMode.LOOP_REVERSED);
		
		jump_pos = new Texture(RESOURCE +"jump_y_up2.png");
		jump_neg = new Texture(RESOURCE +"jump_y_down2.png");
		
		jump_pos_ = asAnimation(jump_pos, 17, 22);
		jump_neg_ = asAnimation(jump_neg, 15, 20);
		
		jump_pos_left = new Texture(RESOURCE +"jump_y_up_left2.png");
		jump_neg_left = new Texture(RESOURCE +"jump_y_down_left2.png");
		
		jump_pos_left_ = asAnimation(jump_pos_left, 17, 22);
		jump_neg_left_ = asAnimation(jump_neg_left, 15, 20);
		
		attack = new Texture(RESOURCE +"swing_sword2-strip.png");
		attack_ = asAnimation(attack, 27, 19);
		
		attack_left = new Texture(RESOURCE +"swing_sword_left2-strip.png");
		attack_left_ = asAnimation(attack_left, 27, 19);
		attack_left_.setPlayMode(PlayMode.REVERSED);
		
		slime = new Texture(RESOURCE +"slime-strip.png");
		slime_ = asAnimation(slime, 128, 104);
		
		background = new Texture(RESOURCE +"maps/mapart/background.png");
		background.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
		
		background2 = new Texture(RESOURCE +"maps/mapart/background2.png");
		background2.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
		
		background2_over = new Texture(RESOURCE +"maps/mapart/background2_over.png");
		background2_over.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
		
		background3 = new Texture(RESOURCE +"maps/mapart/background3.png");
		background3.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
		
		background3_over = new Texture(RESOURCE +"maps/mapart/background3_over.png");
		background3_over.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
		
		forest = new Texture(RESOURCE +"maps/mapart/forest.png");
		forest.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
		
		flaylayout = BoxUtil.getMap(Assets.RESOURCE + "maps/flatlayout.tmx");
		hormap = BoxUtil.getMap(Assets.RESOURCE + "maps/newmap.tmx");
		overworld = BoxUtil.getMap(Assets.RESOURCE + "maps/large_overworld.tmx");
		
		ow_char = new Texture(RESOURCE + "ow_character.png");
		ow_char_ = asAnimation(ow_char, 144/9, 16);
		
		ow_char_down = new Animation(1/6f, new TextureRegion[]{
			ow_char_.getKeyFrames()[1], ow_char_.getKeyFrames()[2]
		});
		ow_char_right = new Animation(1/6f, new TextureRegion[]{
			ow_char_.getKeyFrames()[4], ow_char_.getKeyFrames()[5]
		});
		
		TextureRegion f1, f2;
		f1 = new TextureRegion(ow_char_right.getKeyFrames()[0]);
		f2 = new TextureRegion(ow_char_right.getKeyFrames()[1]);
		
		f1.flip(true, false);
		f2.flip(true, false);
		
		ow_char_left = new Animation(1/6f, new TextureRegion[]{
				f1, f2
		});
		
		ow_char_up = new Animation(1/6f, new TextureRegion[]{
			ow_char_.getKeyFrames()[7], ow_char_.getKeyFrames()[8]
		});
		
	}
	private static Animation asAnimation(Texture t, int width, int height){
		TextureRegion[][] split = TextureRegion.split(t, width, height);
		return new Animation(1/6f, split[0]);
	}
	private static Animation asAnimation(Texture t, int width, int height, float time){
		TextureRegion[][] split = TextureRegion.split(t, width, height);
		return new Animation(time, split[0]);
	}
	public static void dispose(){
		idle.dispose();
	}
}
