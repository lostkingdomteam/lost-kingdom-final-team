package com.shawngl;

import java.util.Stack;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.EllipseMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

public class Level implements Screen {
	
	public Game gameRef;
	public LevelType type;
	
	public Level(Game game, LevelType type, TiledMap map){
		this.gameRef = game;
		this.type = type;
		currentMap = map;
	}
	
	Body playerBody;
	Player player;
	BoxUtil tools;

	boolean debug = false;

	TiledMap currentMap;

	OrthographicCamera cam, background_cam, screen_cam;
	
	public FadeEffect effect;

	World world;
	Stack<Array<Body>> platforms;
	Box2DDebugRenderer debugRenderer;

	SpriteBatch screen, back, graphics;

	OrthogonalTiledMapRenderer mapRender;

	int[] background_layers = new int[] { 0, 1 };
	int[] foreground_layer = new int[] { 2 };
	
	float state = 0f;
	int score = 0;

	@Override
	public void render(float delta) {	
		// render

		cam.position.x = MathUtils.clamp(playerBody.getPosition().x, 10f, ((TiledMapTileLayer)currentMap.getLayers().get(0)).getWidth()/10*16-2);
		cam.position.y = MathUtils.clamp(playerBody.getPosition().y, 8f, ((TiledMapTileLayer)currentMap.getLayers().get(0)).getHeight()/10*16-2);

		cam.update(false);
		
		graphics.setProjectionMatrix(cam.combined);
		back.setProjectionMatrix(background_cam.combined);
		screen.setProjectionMatrix(screen_cam.combined);
		mapRender.setView(cam);

		back.begin();
		
		switch(type){
		case Castle:
			back.draw(Assets.background3, 0, 0, (int) playerBody.getPosition().x / 2, 0, 384, 224);
			back.draw(Assets.background3_over, 0, 0, (int) playerBody.getPosition().x / 2, 95, 384, 224);
			break;
		case Forest:
			back.draw(Assets.background2, 0, 0, MathUtils.clamp((int) playerBody.getPosition().x, 10, ((TiledMapTileLayer)currentMap.getLayers().get(1)).getHeight()/10*16-2), 0, 384, 224);
			back.draw(Assets.background2_over, 0, 0, 0, 0, 384, 224);
			break;
		case Grass:
			back.draw(Assets.background, 0, 0, MathUtils.clamp((int) playerBody.getPosition().x, 10, ((TiledMapTileLayer)currentMap.getLayers().get("collision")).getWidth()/10*16-2)/5, 0, 384, 224);
			back.draw(Assets.forest, 0, 0, MathUtils.clamp((int) playerBody.getPosition().x, 10, ((TiledMapTileLayer)currentMap.getLayers().get(1)).getWidth()/10*16-2)/3, 0, 384, 224);
		case Cave:
			break;
		default:
			break;
		}
		back.end();

		mapRender.render(background_layers);

		graphics.begin();
			for (Entity e : Entity.entities) {
				e.update(delta, graphics);
			}
			DeathEffect.animate(graphics, delta);
		graphics.end();

		mapRender.render(foreground_layer);

		if (debug) {
			debugRenderer.render(world, cam.combined);
		}
		
		screen.begin();
			Assets.FONT.getData().setScale(0.6f, 0.6f);
			Assets.FONT.draw(screen, "SCORE: " + score, 5f, 235f);
			for(int i = 0; i < player.health; i++){
				screen.draw(Assets.heart, i * 35 + 215, 216, 11 * 3, 11 * 2f);
			}
		screen.end();
		// logic
		world.step(1 / 30f, 6, 3);
		
		//Check for flagged deaths and then delete whoever is flagged.
		for (int i = 0; i < Entity.entities.size; i++) {
			if (Entity.entities.get(i).isTaggedDeath) {
				Body body = Entity.entities.get(i).entity;
				new DeathEffect(body.getPosition().x, body.getPosition().y);
				world.destroyBody(body);
				Entity.entities.removeIndex(i);
			}
		}
		
		DeathEffect.removeFinishedAnimation();
		
		if(!effect.done){
			effect.update(screen_cam, delta);
		}
		switchScreenEffect(delta);
	}

	@Override
	public void show() {
		screen = new SpriteBatch();
		back = new SpriteBatch();
		graphics = new SpriteBatch();
		
		world = new World(new Vector2(0, -9.81f), true);
		platforms = new Stack<Array<Body>>();
		debugRenderer = new Box2DDebugRenderer();
		effect = new FadeEffect(false);
		
		tools = new BoxUtil(world, this);
		mapRender = new OrthogonalTiledMapRenderer(currentMap, 2f / Assets.PPM);

		cam = new OrthographicCamera();
		background_cam = new OrthographicCamera();
		screen_cam = new OrthographicCamera();
		screen_cam.setToOrtho(false, 320, 240);
		background_cam.setToOrtho(false, 384, 224);
		cam.setToOrtho(false, 320 / Assets.PPM, 240 / Assets.PPM);

		cam.update();
		background_cam.update();
		screen_cam.update();

		buildCollision(currentMap, world);
		world.setContactListener(new HitHandler(this));
		
		//Assets.level1.play();
	}
	private void switchScreenEffect(float delta){
		if(Player.DEAD && effect.first){
			effect.update(screen_cam, delta);
		}
		else if(Player.DEAD){
			Player.DEAD = false;
			gameRef.setScreen(Platformer.menu);
		}
	}
	
	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
		dispose();
	}

	float scaledRatio = 1 / 8f;

	private void buildCollision(TiledMap map, World world) {
		TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get("collision");
		MapLayer entities_layer = map.getLayers().get("entities");
		MapObjects entities = entities_layer.getObjects();
		BodyDef bDef = new BodyDef();
		PolygonShape shape = new PolygonShape();
		FixtureDef fDef = new FixtureDef();
		fDef.density = 2.5f;
		fDef.isSensor = false;
		fDef.friction = 0f;
		fDef.restitution = 0;

		bDef.type = BodyType.StaticBody;
		Body tileBody = world.createBody(bDef);
		int cellcounter = 0;
		TiledMapTileLayer.Cell cell, nextCell, prevCell;
		int firstCellIndexX = 0, firstCellIndexY = 0;

		for (int j = 0; j < layer.getHeight(); j++) {
			for (int i = 0; i < layer.getWidth(); i++){
				cell = layer.getCell(i, j);
				prevCell = layer.getCell(i - 1, j);
				nextCell = layer.getCell(i + 1, j);

				if (cell != null) {
					cellcounter++;
					if (prevCell == null) {
						firstCellIndexX = i;
						firstCellIndexY = j;
					}

					if (nextCell == null) {
						float width = layer.getTileWidth() * cellcounter * scaledRatio;
						float height = layer.getTileHeight() * scaledRatio;

						shape.setAsBox(width / 2, height / 2,
								new Vector2((firstCellIndexX * layer.getTileWidth() * scaledRatio) + (width / 2),
										(firstCellIndexY * layer.getTileHeight() * scaledRatio) + (height / 2)),
								0);
						fDef.shape = shape;
						fDef.filter.categoryBits = Constants.GROUND_BIT;
						fDef.filter.maskBits = Constants.SKELETON_BIT | Constants.PLAYER_BIT;
						Fixture f = tileBody.createFixture(fDef);
						f.setUserData(Constants.GROUND);
						cellcounter = 0;
					}
				}
			}
		}
		shape.dispose();
		for (int i = 0; i < entities.getCount(); i++) {
			if (entities.get(i).getName().equals("player")) {
				RectangleMapObject pmo = (RectangleMapObject) entities.get(i);
				playerBody = tools.addPlayer(pmo.getRectangle().x * scaledRatio, pmo.getRectangle().y * scaledRatio);
			} 
			else if (entities.get(i).getName().equals("slime")) {
				EllipseMapObject co = (EllipseMapObject) entities.get(i);
				new Slime(tools.addSlime(co.getEllipse().x * scaledRatio, co.getEllipse().y * scaledRatio), this);
			}
			else if (entities.get(i).getName().equals("skeleton")) {
				EllipseMapObject co = (EllipseMapObject) entities.get(i);
				new Skeleton(tools.addSkeleton(co.getEllipse().x * scaledRatio, co.getEllipse().y * scaledRatio), this);
			}
		}

	}
	@Override
	public void dispose() {
		Entity.entities.clear();
		world.dispose();
		//currentMap.dispose();
		debugRenderer.dispose();
		graphics.dispose();
		screen.dispose();
		back.dispose();
		mapRender.dispose();
		System.gc();
	}
}
