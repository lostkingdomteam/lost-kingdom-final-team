package com.shawngl;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;

public class Slime extends Entity {

	Timer timeHandle = new Timer();
	float stateTime = 0f;
	float velx = 1f;
	
	public Slime(Body body, Level l) {
		super(body, l);
		health = 3;
	}

	@Override
	public void update(float delta, SpriteBatch batch) {
		timeHandle.updateTime(delta);

		if (!isFree) {
			blinker.setBlinking(false);
			if (timeHandle.seconds >= 15
					&& (entity.getLinearVelocity().y < 0.01f && entity.getLinearVelocity().y > -0.01f)) {
				timeHandle.seconds = 0;
				jump();
			}

			if (entity.getLinearVelocity().x == 0f) {
				velx = -velx;
			}
			entity.setLinearVelocity(velx, entity.getLinearVelocity().y);
		}
		else{
			damageTimer.time -= delta;
			if(damageTimer.time <= 0){
				isFree = false;
			}
		}
		if(blinker.shouldBlink(delta)){
			return;
		}
		batch.draw(Assets.slime_.getKeyFrame(stateTime += delta, true), entity.getPosition().x - 1.3f,
				entity.getPosition().y - 1f, 2.6f, 2f);

	}

	private void jump() {
		entity.setLinearVelocity(entity.getLinearVelocity().x, 7f);
	}

	@Override
	public void die() {
		worldLevel.score += 100;
		Assets.slimedie.play();
		isTaggedDeath = true;
	}

	@Override
	public void render(float delta, SpriteBatch batch) {
		// TODO Auto-generated method stub
		
	}
}
