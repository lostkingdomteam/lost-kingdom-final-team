package com.shawngl;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
/**
 * The overworld will dictate levels and other places the player will go to.
 * It should be event driven and will display important data along the way.
 * Steps will sometimes apply a random-event w/ plenty of monsters to fight.
 * 
 * @author Game Development
 *
 */
public class Overworld implements Screen{
	
	private Level levelToLoad;
	private final boolean DEBUG = false;
	private Game game;
	FadeEffect effect;
	public Overworld(Game game){
		this.game = game;
	}

	private boolean[][] blocks;
	private Array<Location> locations = new Array<Location>();
	
	OrthographicCamera game_cam, screen_cam;
	OrthogonalTiledMapRenderer mapRender;
	SpriteBatch batch;
	ShapeRenderer sBatch;
	Player p;
	private float distance = 0f;
	private float speed = 32f;
	private Vector2 originalPosition;
	private Animation currentFrame = Assets.ow_char_down;
	
	@Override
	public void show() {
		effect = new FadeEffect(false);
		game_cam = new OrthographicCamera(320, 240);
		screen_cam = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		screen_cam.setToOrtho(true);
		mapRender = new OrthogonalTiledMapRenderer(Assets.overworld);
		batch = new SpriteBatch();
		sBatch = new ShapeRenderer();
		
		if(levelToLoad == null)
			p = new Player(280, 288);
		else
			p.setPostition(p.x, p.y - 1);
		
		constructLocations(Assets.overworld);
		game_cam.update();
	}
	private void constructLocations(TiledMap map){
		TiledMapTileLayer bottom_layer = (TiledMapTileLayer) map.getLayers().get("Bottom Layer");
		TiledMapTileLayer location_layer = (TiledMapTileLayer) map.getLayers().get("Locations");
		TiledMapTileLayer block_layer = (TiledMapTileLayer) map.getLayers().get("Blocks");
		MapObjects properties = map.getLayers().get("LocationProperties").getObjects();
		blocks = new boolean[bottom_layer.getWidth()][bottom_layer.getHeight()];
		for(int i = 0; i < bottom_layer.getWidth(); i++){
			for(int j = 0; j < bottom_layer.getHeight(); j++){
				if(location_layer.getCell(i, j) != null){
					for(int o = 0; o < properties.getCount(); o++){
						RectangleMapObject rmo = (RectangleMapObject) properties.get(o);
						Rectangle r = rmo.getRectangle();
						if(r.getX()/16 == i && r.getY()/16 == j){
							MapProperties prop = rmo.getProperties();
							System.out.println("Adding new map: " + prop.get("map").toString());
							locations.add(new Location(i, j, Assets.RESOURCE +"maps/"+ prop.get("map").toString() + ".tmx" , LevelType.valueOf(prop.get("type").toString())));
							break;
						}
					}
					continue;
				}
				if(block_layer.getCell(i, j) != null){
					blocks[i][j] = true;
					//System.out.println("Added a new block\nTile position is " + i + " | " + j);
				}
				else{
					blocks[i][j] = false;
				}
			}
		}
	}

	@Override
	public void render(float delta){
		game_cam.position.x = MathUtils.round(p.r.x);
		game_cam.position.y = MathUtils.round(p.r.y);
		
		game_cam.update();
		
		mapRender.setView(game_cam);
		sBatch.setProjectionMatrix(game_cam.combined);
		batch.setProjectionMatrix(game_cam.combined);
		
		mapRender.render();
		
		if(Gdx.input.isKeyPressed(Keys.A)){
			if(!p.down && !p.up && !p.right){
				if(!blocks[p.x-1][p.y]){
					if(!p.left){
						currentFrame = Assets.ow_char_left;
						originalPosition = new Vector2(p.r.x, p.r.y);
					}
					p.left = true;
				}
			}
		}
		else if(Gdx.input.isKeyPressed(Keys.D)){
			if(!p.down && !p.up && !p.left){
				if(!blocks[p.x+1][p.y]){
					if(!p.right){
						currentFrame = Assets.ow_char_right;
						originalPosition = new Vector2(p.r.x, p.r.y);
					}
					p.right = true;
				}
			}
		}
		else if(Gdx.input.isKeyPressed(Keys.W)){
			if(!p.down && !p.left && !p.right){
				if(!blocks[p.x][p.y+1]){
					if(!p.up){
						currentFrame = Assets.ow_char_up;
						originalPosition = new Vector2(p.r.x, p.r.y);
					}
					p.up = true;
				}
			}
		}
		else if(Gdx.input.isKeyPressed(Keys.S)){
			if(!p.left && !p.up && !p.right){
				if(!blocks[p.x][p.y-1]){
					if(!p.down){
						currentFrame = Assets.ow_char_down;
						originalPosition = new Vector2(p.r.x, p.r.y);
					}
					p.down = true;
				}
			}
		}
		
		if(p.right || p.left || p.up || p.down){
			distance += MathUtils.clamp(speed * delta, 0, 16f);
			if(distance >= 16f){
				distance = 0f;
				
				if(p.left){
					p.r.x = originalPosition.x - 16f;
					p.x--;
				}
				else if(p.right){
					p.r.x = originalPosition.x + 16f;
					p.x++;
				}
				else if(p.up){
					p.r.y = originalPosition.y + 16f;
					p.y++;
				}
				else if(p.down){
					p.r.y = originalPosition.y - 16f;
					p.y--;
				}
				
				p.left = false;
				p.right = false;
				p.up = false;
				p.down = false;
				System.out.println(p.x + " | " + p.y + ". Location is " + blocks[p.x][p.y] + "(" + p.x + " | " + p.y + ")");
			}
		}
		if(p.left){
			p.r.x -= MathUtils.clamp(speed * delta, 0, distance);
		}
		else if(p.right){
			p.r.x += MathUtils.clamp(speed * delta, 0, distance);
		}
		else if(p.up){
			p.r.y += MathUtils.clamp(speed * delta, 0, distance);
		}
		else if(p.down){
			p.r.y -= MathUtils.clamp(speed * delta, 0, distance);
		}
		for(Location loc : locations){
			if(p.r.overlaps(loc.r)){
				if(!touchedLocationSquare){
					levelToLoad = loc.level;
					effect = new FadeEffect(true);
					touchedLocationSquare = true;
				}
			}
		}
		if(DEBUG){
			sBatch.begin(ShapeType.Line);
				for(Location loc : locations){
					sBatch.rect(loc.r.x, loc.r.y, 16, 16);
				}
				for(int i = 0; i < blocks.length; i++){
					for(int j = 0; j < blocks[i].length; j++){
						if(blocks[i][j]){
							sBatch.rect(i * 16, j * 16, 16, 16);
						}
					}
				}
				sBatch.rect(p.r.x, p.r.y, 16, 16);
			sBatch.end();
		}
		
		batch.begin();
			batch.draw(currentFrame.getKeyFrame(stateTimeAnimation += delta, true), p.r.x, p.r.y+3f);
		batch.end();
		
		if(!effect.done){
			effect.update(screen_cam, delta);
		}
		switchScreenEffect(delta);
	}
	
	boolean touchedLocationSquare = false;
	
	private void switchScreenEffect(float delta){
		if(touchedLocationSquare && effect.first){
			effect.update(screen_cam, delta);
		}
		else if(touchedLocationSquare){
			touchedLocationSquare = false;
			game.setScreen(levelToLoad);
		}
	}

	float stateTimeAnimation = 0f;
	
	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	private class Player{
		Rectangle r;
		boolean up, down, left, right = false;
		int x, y;
		
		Player(int x, int y){
			r = new Rectangle(x * 16, y * 16, 16, 16);
			this.x = x;
			this.y = y;
		}
		void setPostition(int x, int y){
			this.x = x;
			this.y = y;
			r.x = x * 16;
			r.y = y * 16;
		}
	}
	private class Location{
		
		Level level;
		Rectangle r;
		
		Location(int x, int y, String mapFileLocation, LevelType type){
			r = new Rectangle(x * 16, y * 16, 16, 16);
			level = new Level(game, type, BoxUtil.getMap(mapFileLocation));
		}
	}
}
