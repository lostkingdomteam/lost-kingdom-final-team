package com.shawngl;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

public class FadeEffect {

	float transition;
	boolean first, done;

	ShapeRenderer box;

	// Checks if this is the first screen.
	public FadeEffect(boolean first) {
		this.first = first;
		this.done = false;
		this.box = new ShapeRenderer();
		if(first)
			transition = 0f;
		else
			transition = 1f;
	}

	public void update(OrthographicCamera cam, float delta) {
		box.setProjectionMatrix(cam.combined);
		Gdx.gl.glEnable(GL20.GL_BLEND);
		box.begin(ShapeType.Filled);
			if(first){
					box.setColor(0f, 0f, 0f, transition+=delta);
					if(transition >= 1f){
						first = false;
					}
				}
			else{
				box.setColor(0f, 0f, 0f, transition-=delta);	
				if(transition <= 0f){
					done = true;
				}
			}
		box.rect(0, 0, cam.viewportWidth, cam.viewportHeight);
		box.end();
		Gdx.gl.glDisable(GL20.GL_BLEND);
	}
}
